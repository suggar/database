"""All stuff that provides database connection."""
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from suggar_db.exceptions import SuggarCoreException
from suggar_db.utils import Singleton


class DBConnection(metaclass=Singleton):
    """Provide tool for managing database connection."""

    def __init__(self, db_uri=None):
        self.db_uri = db_uri
        self.db_engine = None
        self.SessionClass = None
        self.BaseModel = declarative_base()

    def init_engine(self):
        """Initialize engine with provided valid database URI."""
        self.db_engine = create_engine(self.db_uri)
        self.SessionClass = sessionmaker(bind=self.db_engine)
        self.BaseModel.bind = self.db_engine

    def create_session(self):
        """Create session assigned to current database."""
        if not self.SessionClass:
            raise SuggarCoreException('Unbound session creation detected')

        return DBSession(self.SessionClass)


class DBSession():
    """Provide session entity to make database queries."""

    def __init__(self, session_class):
        self._current_session = None
        self._SessionClass = session_class

    def __enter__(self):
        """Create session object."""
        session = self._SessionClass()
        self._current_session = session
        return session

    def __exit__(self, exc_type, exc_val, exc_tb):
        """Make sure session is closed after all."""
        if exc_val:
            self._current_session.rollback()
        else:
            self._current_session.commit()

        self._current_session.close()

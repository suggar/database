"""Contain db-related exceptions."""


class SuggarCoreException(Exception):
    """Common exception."""

    message = ''

    def __init__(self, message):
        Exception.__init__(self, message)
        self.message = message

    def __str__(self):
        """Return string representation of error."""
        return self.message

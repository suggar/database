"""Contain utilities that make life easier."""


class Singleton(type):
    """Metaclass that is ment to be used in singletons."""

    _instances = {}

    def __call__(cls, *args, **kwargs):
        """Get instance of needed class."""
        if cls not in cls._instances:
            cls._instances[cls] = (
                super(Singleton, cls).__call__(*args, **kwargs))
        return cls._instances[cls]

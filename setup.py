"""Settings for proper package installation."""
from setuptools import setup

setup(
    name='suggar_db',
    version='0.1.0',
    description='SQLAlchemy wrapper',
    url='https://gitlab.com/suggar_db/database',
    author='Nef1k',
    author_email='nef1k@outlook.com',
    license='MIT',
    install_requires=[
        'sqlalchemy==1.2.8'],
    packages=[
        'suggar_db'],
    zip_safe=False)
